// Diseño:      alarma
// Archivo:     alarm_tb.v
// Descripción: Alarma sencilla para un automovil
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       08/11/2018 (versión inicial)

/*
   Lección 3.1: Ejemplos combinacionales. Alarma de automovil.

   Este archivo contiene un banco de pruebas para el módulo alarm.
*/

`timescale 1ns / 1ps

module test ();

    // Entradas
    reg door, engine, lights;

    // Salidas
    wire siren;

    // Instancia de la unidad bajo test (UUT)
    /* Es una buena práctica usar una línea para conectar cada señal. De
     * esta forma es más fácil ver si se han conectado todas las señales y,
     * también, hacer futuras modificaciones en el diseño, como quitar o
     * añadir señales a los módulos. */
    alarm uut (
        .door(door),
        .engine(engine),
        .lights(lights),
        .siren(siren)
        );

    // Inicialización de entradas y control de la simulación
    initial begin
        // Iniciamos las entradas
        door = 0;     // cerrada
        engine = 0;   // apagado
        lights = 0;   // apagadas

        // Genera formas de onda
        $dumpfile("alarm_tb.vcd");
        $dumpvars(0,test);

        // Pruebas del circuito
        /* Cambiamos las señales de forma que el circuito debe activar la
         * salida (suena la alarma). Comprobamos los dos casos de interés:
         *   - pueta abierta con el motor encendido.
         *   - puerta abierta con motor apagado y luces encendidas. */

        // Puerta abierta y motor encendido
        #10 engine = 1;     // arrancamos el motor
        #10 door = 1;       // abrimos la puerta
                            // aquí debe sonar la alarma
        #10 door = 0;       // cerramos la puerta
                            // aquí debe dejar de sonar la alarma
        #10 engine = 0;     // apagamos el motor
        #10 lights = 1;     // encendemos las luces
        #10 door = 1;       // abrimos la puerta
                            // aquí debe sonar la alarma
        #10 lights = 0;     // apagamos las luces
                            // aquí debe dejar de sonar la alarma
        #10 door = 0;       // cerramos la puerta

        // Finalizamos la simulación
        #20 $finish;
    end

endmodule // test

/*
   EJERCICIIO

   1. Compila la lección con:

        $ iverilog alarm.v alarm_tb.v

   2. Simula el diseño con:

        $ vvp a.out

   3. Observa la formas de onda resultado de la simulación con:

        $ gtkwave alarm_tb.vcd &

      Para interpretar mejor el resultado, selecciona las señales del módulo
      "uut" (unidad bajo test) en el orden: engine, door, lights and siren.
      Comprueba que la salida "sirena" tiene un valor correcto que corresponda
      con el enunciado del problema.

   4. Esta alarma puede llegar a ser molesta. Añada una entrada de control de
      activación de la alarma (puede llamarla "control") de forma que si
      control=0 la alarma permanece desactivada y con control=1 funciona
      normalmente. Modifique el banco de pruebas para probar esta nueva
      funcionalidad y simule y corrija el circuito hasta que opere
      correctamente.
*/
