// Diseño:      primos
// Archivo:     primes_tb.v
// Descripción: Detector de números primos
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       09/11/2018 (versión inicial)

/*
   Lección 3.2: Detector de números primos de 4 bits.

   Este archivo contiene un banco de pruebas para el módulo 'primes'.
*/

`timescale 1ns / 1ps

// Tiempo base para facilitar la definición de patrones de test
`define BTIME 10

module test ();

    // Entrada
    /* La señal que genera las entradas para nuestro circuito es una variable
     * de 4 bits. */
    reg [3:0] x;

    // Salidas
    wire z;

    // Instancia de la unidad bajo test (UUT)
    /* Las señales de múltiples bits se conectan igual que las de un solo bit
     */
    primes uut (.x(x), .z(z));

    /* El siguiente proceso "initial" inicia los patrones de test e incluye
     * las directivas de simulación */
    initial begin
        // Iniciamos la entrada
        x = 4'b0000;
        /* Iniciamos x a cero expresando el valor '0' en binario e indicando
         * expresamente que se trata de un dato de 4 bits. El formato completo
         * para expresar números en Verilog es:
         *   <número de bits>'<base><valor>
         * donde <base> puede ser, entre otros:
         *   b: binario
         *   o: octal
         *   h: hexadecimal
         *   d: decimal
         * Si se omite <base> se considera decimal y si se omite el
         * número de bits se consideran al menos 32 bits, extendiendo el
         * número de bits según sea necesario para realizar la operación.
         * En general, si el el número de bits es conocido, es recomendable
         * indicarlo expresamente en las constantes.
         * Alternativamente, x podría iniciarse como:
         *   x = 4'h00      // hexadecimal
         *   x = 'b0000     // binario, se omite el número de bits
         *   x = 0          // se omite la base y el número de
         *                  // bits. Se asume el valor cero decimal
         *                  // y el número suficiente de bits para
         *                  // asignar x a cero.
         */

        // Genera formas de onda
        $dumpfile("primes_tb.vcd");
        $dumpvars(0,test);

        // Genera salida en formato texto
        /* En este diseño, la salida de texto es muy conveniente porque nos
         * permite comprobar la correcta operación del circuito de forma
         * más rápida y directa que con las formas de onda. */
        $display("x   z");          // cabeceras
        $display("-----");
        $monitor("%d  %b", x, z);

        // Finalizamos la simulación
        /* Fijamos el final de la simulación en función de BTIME. El
         * valor 16*`BTIME es suficiente para que las entradas recorran
         * todos los posibles casos */
        #(16*`BTIME) $finish;
    end

    // Generamos patrones de entrada
    /* Para conseguir que las entradas recorran todos los posibles valores
     * basta con incrementar x cada cierto tiempo (`BTIME). Las variables
     * multibit pueden tratarse como números enteros y aplicarles operadores
     * aritméticos. */
    always
        #(`BTIME) x = x + 1;

endmodule // test

/*
   EJERCICIIO

   1. Compila la lección con:

        $ iverilog primes.v primes_tb.v

   2. Simula el diseño con:

        $ vvp a.out

      Observe la salida del texto del terminal (o consola, etc.) y compruebe
      que los resultados son los esperados.

   3. Observa las formas de onda resultado de la simulación con:

        $ gtkwave primes_tb.vcd &

      Gtkwave probablemente represente la señal 'x' como un número
      hexadecimal. Puede cambiar el formato de representación con la
      opción "Data Format" en el menú contextual de la variable para, por
      ejemplo, elegir una representación decimal. También puede ver las
      formas de onda de cada bit por separado haciendo doble click sobre
      el nombre de la señal.

   4. La sentencia 'case' permite indicar varios valores de la expresión en
      cada una de sus entradas. Por ejemplo:

        case(data)
        1,3,5:   odd = 1'b1;
        0,2,4:   even = 1'b1;
        endcase

      Modifique el diseño del módulo 'primes' empleando este formato y
      simplifique la sentencia 'case'.
      ¿Cree que esta simplificación hará que la simulación del circuito sea
      más eficiente? ¿Y la implementación automática del circuito?

   5. Usando este ejemplo como modelo, diseñe un circuito combinacional que
      tome un valor 'x' de 5 bits como entrada, y active una salida 'z'
      cuando el número es un múltiplo de 7. Realice un banco de pruebas para
      comprobar la operación del circuito.
*/
