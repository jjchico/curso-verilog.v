// Diseño:      uregister
// Archivo:     register.v
// Descripción: Registro universal
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       11/06/2010 (versión inicial)

/*
   Lección 7.1: Registro universal.

   Los elementos de memoria (biestables) suelen usarse en grupos o vectores
   representando datos de varios bits, denominados "registros". Exite una
   variedad de operaciones útiles que pueden realizarse sobre registros como:
   - Carga de un dato (en serie o en paralelo).
   - Lectura de un dato (en serie o paralelo).
   - Puesta a cero o "clear".
   - Puesta a uno o "preset".
   - Desplazamiento del dato (a derecha o izquierda).

   Los registros pueden tener líneas de entrada para todos sus bits de forma
   que una palabra completa puede ser escrita en una sola operación (escritura
   en paralelo). De forma similar, pueden haber líneas de salida para todos
   los bits de forma que se puede leer el contenido completo del registro
   al mismo tiempo (lectura en paralelo).

   En las operaciones de desplazamiento se escribe un bit en el registro a
   través de una entrada serie, mientras que el resto de los bits son
   desplazados a la posición adyacente a la derecha o izquierda (según el tipo
   de desplazamiento) lo que permite cargar un nuevo dato en el registro
   mediante desplazamientos sucesivos (escritura serie). En este caso, el
   contenido del registro puede ser leído bit a bit usando únicamente una
   línea de salida (salida serie).

   En esta lección diseñaremos un registro sobre el que pueden realizarse
   diversas operaciones, a menudo llamados "registros universales". Estos
   registros son muy típicos en implementaciones en chips MSI (Medium Scale
   of Integration) para contar con con dispositivos muy versátiles que pueden
   usarse en diferentes circuitos. Cuando se usa Verilog u otros HDL's, sólo
   registros especiales se suelen definir en módulos independientes mientras
   que la mayoría de registros son simples variables dentro de otros módulos
   más complejos.

   En esta lección se muestra, además, el uso del operador de combinación de
   señales ({}) para implementar operaciones de desplazamiento.
*/

`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////
// Registro Universal                                                   //
//////////////////////////////////////////////////////////////////////////
/*
   El registro es controlado por varias entrada de control que indican la
   operación a realizar con un cierto orden de prioridad. Las operaciones son:

    load shr shl    Operación
    --------------------------
      0   0   0    Inhibición
      1   x   x    Carga de dato
      0   1   x    Desplazamiento a la derecha
      0   0   1    Desplazamiento a la izquierda

   Por otro lado, dispone de las siguientes entradas y salidas:
       clk: señal de reloj
       xr:  entrada del bit más significativo en desplazamiento a la derecha
       xl:  entrada del bit más significativo en desplazamiento a la izquierda
       x:   entrada de dato para la carga de dato
       q:   salida del dato almacenado
 */

module uregister #(
    // anchura del registro
    /* La anchura del registro está parametrizada con un valor por
    * defecto de 8 bits */
    parameter W = 8
    )(
    input wire clk,         // reloj
    input wire load,        // carga de dato en paralelo
    input wire shr,         // desplazamiento a la derecha
    input wire shl,         // desplazamiento a la izquierda
    input wire xr,          // entrada serie para shr
    input wire xl,          // entrada serie para shl
    input wire [W-1:0] x,   // entrada de dato para carga en paralelo
    output [W-1:0] z        // contenido del registro (estado)
    );

    /* Señal interna para almacenar el estado. La salida 'z' se asignará
     * directamente con el valor del estado. Técnicamente, en Verilog no
     * es necesario definir una señal interna para el estado ya que la propia
     * señal de salida podría hacer esta función, pero hacerlo aporta
     * claridad al código y aporta mayor similitud con el estilo de otros
     * HDL's. */
    reg [W-1:0] q;      // estado

    always @(posedge clk) begin
        if (load)
            q <= x;
        else begin
            if (shr)
                q <= {xr, q[W-1:1]};
            else if (shl)
                q <= {q[W-2:0], xl};
        end
    end

    assign z = q;

endmodule // uregister

/*
   (Esta lección continúa en el archivo 'register_tb.v').
*/
