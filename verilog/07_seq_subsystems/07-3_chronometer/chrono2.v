// Diseño:      chrono
// Archivo:     chrono2.v
// Descripción: Cronómetro digital. Versión 2
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       14/06/2010 (versión inicial)

/*
   Lección 7.3: Cronómetro digital.

   Este archivo contiene un diseño alternativo al módulo 'chrono1' empleando
   un sólo proceso para calcular el nuevo valor de todas las cifras.
*/

`timescale 1 ns / 1 ps

//////////////////////////////////////////////////////////////////////////
// Cronómetro                                                           //
//////////////////////////////////////////////////////////////////////////

module chrono2 #(
    // Frecuencia del reloj del sistema en Hz
    parameter SYSFREQ = 50000000
    )(
    input wire clk,         // reloj
    input wire cl,          // puesta a cero (activo en alto)
    input wire start,       // habilitación (activo en alto)
    output reg [3:0] h0,    // unidades de centésimas
    output reg [3:0] h1,    // decenas de centésimas
    output reg [3:0] s0,    // unidades de segundo
    output reg [3:0] s1     // decenas de segundo
    );

    // Ajuste del divisor de frecuencia. Milisegundos
    localparam FDIV = SYSFREQ/100;

    /* Con 24 bits para el divisor de frecuencia es posible emplear relojes
     * externos de hasta 1,6GHz. Puedes ahorrar bits si sabes lo que haces. */
    reg [23:0] dcount;    // Contador del divisor de frecuencia

    // Divisor de frecuencia
    always @(posedge clk) begin
        if (cl == 1)
            dcount <= FDIV - 1;
        else if (start == 1)
            if (dcount == 0)
                dcount <= FDIV - 1;
            else
                dcount <= dcount - 1;
    end

    // Salida de habilitación del divisor de frecuencia
    assign cnt = ~|dcount;

    // Cronómetro
    /* Con cada flanco, un solo proceso calcula el nuevo valor de todas las
     * cifras del contador. Se empieza por las centésimas de segundo. Si
     * no están en su valor máximo, se incrementan. Si están en el valor
     * máximo, se ponen a cero y se incrementa el dígito siguiente, y así
     * sucesivamente. */
    always @(posedge clk) begin
        if (cl) begin
            /* Si se activa 'cl' hacemos una puesta a cero. */
            h0 <= 0;
            h1 <= 0;
            s0 <= 0;
            s1 <= 0;
        end else if (cnt) begin
            /* Si se activa 'cnt' hay que incrementar la cuenta. */
            if (h0 < 9) // incremento normal
                h0 <= h0 + 1;
            else begin      // puesta a cero y comprobamos siguiente cifra
                h0 <= 0;
                if (h1 < 9) // incremento normal
                    h1 <= h1 + 1;
                else begin  // puesta a cero y comprobamos siguiente cifra
                    h1 <= 0;
                    if (s0 < 9) // incremento normal
                        s0 <= s0 + 1;
                    else begin  // puesta a cero y comprobamos siguiente cifra
                        s0 <= 0;
                        if (s1 < 5) // incremento normal
                            s1 = s1 + 1;
                        else        // puesta a cero
                            s1 <= 0;
                    end
                end
            end
        end
    end

endmodule // chrono2
