// Diseño:      chrono
// Archivo:     chrono_tb.v
// Descripción: Cronómetro digital. Banco de pruebas.
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       14/06/2010 (versión inicial)

/*
   Lección 7.3: Cronómetro digital.

   Este archivo contiene un banco de pruebas para el módulo de cronómetro
   en el archivo 'chrono1.v'. El aspecto más difícil de este banco de pruebas
   radica en que, para una frecuencia del reloj del sistema elevada, podemos
   necesitar muchos ciclos de reloj para simular unos cuantos segundos de la
   operación del cronómetro. Por ejemplo, con un reloj del sistema de 50MHz
   necesitamos simular mil millones (10^9) ciclos de reloj para un tiempo
   simulado de sólo 2 segundos. Afortunadamente la frecuencia del sistema es
   un parámetro en nuestro diseño por lo que, para la simulación, instanciaremos
   una unidad de test con una frecuencia del sistema de sólo 1000Hz, esto es,
   1000 ciclos de reloj por cada segundo simulado.

   Por otro lado, para aplicar las señales de entrada en los tiempo apropiados
   de forma sencilla emplearemos profusamente el operador de control temporal
   '@' y la directiva 'repeat'.
*/

`timescale 1 ns / 1 ps

// Frecuencia del reloj del sistema en Hz.
/* Macro con la frecuencia del sistema. Será pasada al módulo 'chrono' como
 * el parámetro del mismo nombre. El uso de una macro aquí facilita poder
 * cambiar el valor de la frecuencia con facilidad. */
`define SYSFREQ 1000

// Banco de pruebas

module test ();

    reg clk = 0;    // reloj
    reg cl = 0;     // puesta a cero (activo en alto)
    reg start = 0;  // habilitación (activo en alto)
    wire [3:0] h0;  // unidades de centésimas
    wire [3:0] h1;  // decenas de centésimas
    wire [3:0] s0;  // unidades de segundo
    wire [3:0] s1;  // decenas de segundo

    // Circuito bajo test
    /* Instanciamos el cronómetro ajustado a una frecuencia de reloj del
     * sistema SYSFREQ. */
    chrono1 #(.SYSFREQ(`SYSFREQ)) uut(.clk(clk), .cl(cl), .start(start),
                            .h0(h0), .h1(h1), .s0(s0), .s1(s1));

    // Salidas y control de la simulación
    initial begin
        // Generamos formas de onda para visualización posterior
        /* Este banco de pruebas puede, potencialmente, generar una gran
         * cantidad de datos de simulación, por lo que usamos el formato FST
         * en lugar del clásico VCD. Los archivos FST son mucho más compactos
         * que los VCD (diez veces o más) y más rápidos de generar y de leer
         * por los visualizadores de ondas que el antiguo formato VCD. Aquí
         * sólo definimos el nombre del archivo. La capacidad de generar
         * resultados en formato FST depende del simulador. Tanto Icarus
         * Verilog como Gtkwave soportan este formato. */
        $dumpfile("chrono_tb.fst");
        $dumpvars(0, test);

        // Generamos salida en formato texto
        $display("cl start  SS.CC");
        $monitor("%b  %b      %h%h.%h%h",
                   cl, start, s1, s0, h1, h0);
    end

    // Señal de reloj periódica
    /* Generamos una señal de reloj de frecuencia 'SYSFREQ' teniendo en
     * cuenta que la unidad de tiempo del simulador es ns (fijada por la
     * directiva 'timescale' más arriba) y que 'SYSFREQ' está en Hz. */
    always
        #(1e9 / `SYSFREQ / 2) clk = ~clk;

    // Entradas
    /* Sincronizamos los cambios de las entradas de control con los flancos
     * de bajada del reloj. Las entradas hacen una puesta a cero inicial,
     * una cuenta durante unos segundos, una parada y una puesta a cero
     * durante la cuenta. */
    initial begin
        @(negedge clk)
        cl = 1;                 // puesta a cero
        @(negedge clk)
        cl = 0;                 // comienzo
        start = 1;
        /* Esperamos 7 segundos */
        repeat(7 * `SYSFREQ) @(negedge clk);
        start = 0;              // parada
        /* Esperamos 2 segundos */
        repeat(2 * `SYSFREQ) @(negedge clk);
        start = 1;              // continuar
        repeat(6 * `SYSFREQ) @(negedge clk);
        cl = 1;                 // puesta a cero sin parar
        repeat(1 * `SYSFREQ) @(negedge clk);
        cl = 0;
        repeat(3 * `SYSFREQ) @(negedge clk);
        $finish;
    end

endmodule // test

/*
   EJERCICIOS

   1. Compila y simula los ejemplos con:

        $ iverilog chrono1.v chrono_tb.v
        $ vvp a.out -fst

      El simulador generará un archivo 'chrono_tb.fst' con las formas de onda
      resultado de la simulación. El formato fst (generado gracias a la
      opción '-fst' de 'vvp') es más eficiente que el vcd cuando se trata de
      simulaciones que generan muchos datos.

   2. Visualiza e interpreta los resultados con:

        $ gtkwave chrono_tb.fst

   3. Modifica el valor de la macro SYSFREQ a un valor mayor (100000 o 1000000)
      y vuelve a ejecutar la simulación. Observa el tiempo que tarda en
      ejecutarse la simulación y el tamaño del archivo de datos generado.
      Visualiza los resultados y observa la diferencia con diferentes valores
      de SYSFREQ.

   4. Estudia el diseño del módulo 'chrono2' en el archivo 'chrono2.v'. Usa el
      banco de pruebas para ver si funciona igual que 'chrono1'. ¿Qué ventajas
      y/o inconvenientes tiene 'chrono2' sobre 'chrono1'?

   5. Realiza un diseño 'chrono3' con el mismo comportamiento que 'chrono1' pero
      empleando una descripción estructural con las siguientes directrices:

      - El módulo 'chrono3' debe tener exactamente las mismas entradas y
        salidas que 'chrono1' (se puede simular con el mismo banco de pruebas).

      - Describe estos elementos en módulos independientes: divisor de
        frecuencia, contador de centésimas, contador de décimas, contador de
        unidades y contador de decenas. Define las entradas y salidas de estos
        módulos de forma conveniente.

      - Construye el módulo 'chrono3' como interconexión de los módulos
        anteriores, junto con la lógica adicional que sea necesaria.

   6. Simula el módulo 'chrono3' con el mismo banco de pruebas que 'chrono1' y
      comprueba que operan de forma idéntica.
*/
