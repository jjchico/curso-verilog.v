// Diseño:      chrono
// Archivo:     chrono1.v
// Descripción: Cronómetro digital
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       14/06/2010 (versión inicial)

/*
   Lección 7.3: Cronómetro digital.

   En esta lección haremos un diseño algo más complejo: un cronómetro digital
   con centésimas y segundos con funciones de inicio y parada de la cuenta
   y de puesta a cero.

   Cuando los sistemas son algo complejos es necesario hacer una organización
   inicial del problema que ayude a realizar la descripción. En este sentido
   suele ser útil separar el problema en bloques más o menos independientes y
   describirlos por separado para luego conectarlos entre si. Esto requiere
   en general describir cada bloque en un módulo Verilog independiente para
   luego unir todo el sistema mediante una descripción estructural.

   En este ejemplo usaremos un único módulo, pero aún así pensaremos en varios
   bloques a la hora de realizar la descripción. Así, la descripción del
   sistema se organiza en base a los siguientes puntos:

   - La señal de reloj del sistema (ck) tendrá en general una frecuencia mayor
     a 100Hz por lo que necesitaremos un divisor de frecuencia que habilite
     la cuenta sólo cuando haya transcurrido un número suficiente de ciclos del
     reloj del sistema. Para poder adaptar el diseño a diversas condiciones, la
     frecuencia del reloj del sistema será un parámetro configurable a partir
     del cual el divisor de frecuencia generará la señal de habilitación al
     ritmo adecuado.

   - El sistema se compone básicamente de cuatro contadores:
     * H0: centésismas de segundo (unidades de 0 a 9)
     * H1: centésimas de segundo (decenas de 0 a 9)
     * S0: segundos (unidades de 0 a 9)
     * S1: segundos (decenas de 0 a 5)

   - Cada contador se describirá por separado (en su propio proceso). Cada
     contador se incrementará cuando la cuenta sea habilitada por el divisor
     de frecuencia y sólo si los contadores menos significativos han llegado
     al último estado de cuenta.

   - Se generará una señal de fin de cuenta para cada contador para que pueda
     ser empleada por los contadores más significativos.

   - Cada contador se pondrá a cero cuando se active una señal general de
     puesta a cero (cl).

   - Una señal 'start' controlará el inicio y la parada de la cuenta actuando
     sobre el divisor de frecuencia. La cuenta sólo se produce cuando start=1.
*/

`timescale 1 ns / 1 ps

//////////////////////////////////////////////////////////////////////////
// Cronómetro                                                           //
//////////////////////////////////////////////////////////////////////////

module chrono1 #(
    // Frecuencia del reloj del sistema en Hz
    parameter SYSFREQ = 50000000
    )(
    input wire clk,         // reloj
    input wire cl,          // puesta a cero (activo en alto)
    input wire start,       // habilitación (activo en alto)
    output reg [3:0] h0,    // unidades de centésimas
    output reg [3:0] h1,    // decenas de centésimas
    output reg [3:0] s0,    // unidades de segundo
    output reg [3:0] s1     // decenas de segundo
    );

    // Ajuste del divisor de frecuencia
    /* FDIV, calculado a partir de SYSFREQ, es el número de ciclos del reloj
     * del sistema que corresponden a una centésima de segundo. Es el
     * número de ciclos que tiene que contar el divisor de frecuencia.
     * Los parámetros locales (localparm) sólo tienen validez dentro del
     * módulo en que se definen y no pueden redefinirse al instanciar el
     * componente. */
    localparam FDIV = SYSFREQ/100;

    /* Con 24 bits para el divisor de frecuencia es posible emplear relojes
     * externos de hasta 1,6GHz. Puedes ahorrar bits si sabes lo que haces. */
    reg [23:0] dcount;    // Contador del divisor de frecuencia

    /* Señales internas de fin de cuenta para cada contador que lo necesita. */
    wire h0end;
    wire h1end;
    wire s0end;

    // Divisor de frecuencia
    /* El divisor de frecuencia no es más que un contador descendente que
     * activa una señal de habilitación de cuenta para el resto de contadores
     * cada vez que llega a cero. Al llegar a cero es iniciado a FDIV-1,
     * lo que hace que active la señal de habilitación general 'cnt' una
     * vez cada centésima de segundo. Una parada de la cuenta (start=0) hace
     * que el divisor de frecuencia se detenga, y por lo tanto se detiene
     * el cronómetro. Una puesta a cero (cl=1) hace que el divisor de
     * frecuencia cargue su valor inicial. */
    always @(posedge clk) begin
        if (cl == 1)
            dcount <= FDIV - 1;
        else if (start == 1)
            if (dcount == 0)
                dcount <= FDIV - 1;
            else
                dcount <= dcount - 1;
    end

    /* Señal de habilitación generada cuando el divisor llega a cero */
    assign cnt = ~|dcount;

    // Contador de unidades de centésimas (H0)
    always @(posedge clk) begin
        if (cl)
            /* Si se activa 'cl' hacemos una puesta a cero. */
            h0 <= 0;
        else if (cnt) begin
            /* Si se activa 'cnt' hay que incrementar la cuenta. */
            if (h0end)
                /* Si estábamos en el último estado de cuenta pasamos
                 * al estado 0. Aprovechamos las señal 'h0end' generada
                 * más abajo para evitar usar otra comparación. */
                h0 <= 0;
            else
                /* Si no hemos llegado al último estado de
                 * cuenta, incrementamos normalmente. */
                h0 <= h0 + 1;
        end
    end

    /* Generamos la señal de fin de cuenta del contador de centésimas */
    assign h0end = (h0 == 9)? 1:0;

    // Contador de decenas de centésimas (H1)
    /* El resto de contadores son muy similares, cambiando ligeramente las
     * condiciones que disparan la operación de cuenta. */
    always @(posedge clk) begin
        if (cl)
            h1 <= 0;
        else if (cnt & h0end) begin
            /* Contamos solo si se ha habilitado la cuenta (cnt=1) y
             * el contador anterior está en su último estado de cuenta
             * (h0end=1). Luego incrementamos H1 como hicimos para H0,
             * teniendo en cuenta el fin de cuenta. */
            if (h1end)
                h1 <= 0;
            else
                h1 <= h1 + 1;
        end
    end

    /* Señal de fin de cuenta de H1 */
    assign h1end = (h1 == 9)? 1:0;

    // Contador de unidades de segundo (S0)
    always @(posedge clk) begin
        if (cl)
            s0 <= 0;
        else if (cnt & h1end & h0end) begin
            /* Sólo si hay habilitación y todos los contadores
             * anteriores en fin de cuenta */
            if (s0end)
                s0 <= 0;
            else
                s0 <= s0 + 1;
        end
    end

    /* Señal de fin de cuenta de S0 */
    assign s0end = (s0 == 9)? 1:0;

    // Contador de decenas de segundo (S1)
    always @(posedge clk) begin
        if (cl)
            s1 <= 0;
        else if (cnt & s0end & h1end & h0end) begin
            if (s1end)
                s1 <= 0;
            else
                s1 <= s1 + 1;
        end
    end

    assign s1end = (s1 == 5)? 1:0;

endmodule // chrono

/*
   Esta lección continúa en el archivo 'chrono_tb.v'.
*/
