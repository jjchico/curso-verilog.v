// Diseño:      counter
// Archivo:     counter_tb.v
// Descripción: Contador reversible. Banco de pruebas.
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       11/06/2010 (versión inicial)

/*
   Lección 7.2: Contador reversible.

   Este archivo contiene un banco de pruebas para el contador diseñado en
   'counter.v'.
*/

`timescale 1ns / 1ps

// Banco de pruebas

module test ();

    reg clk = 0;    // reloj
    reg cl = 0;     // puesta a cero
    reg en = 0;     // habilitación
    reg ud = 0;     // cuenta hacia arriba (0) o abajo (1)
    wire [3:0] z;   // estado de cuenta
    wire c;         // señal de fin de cuenta

    // Instanciación de la unidad bajo test
    /* Instanciamos un contador de 4 bits (módulo 16), no el predeterminado
     * de 8 bits. */
    counter #(4) uut(.clk(clk), .cl(cl), .en(en), .ud(ud), .z(z), .c(c));

    // Salidas y control de la simulación
    initial begin
        // Generamos formas de onda para visualización posterior
        $dumpfile("counter_tb.vcd");
        $dumpvars(0, test);

        /* En esta ocasión no generamos salidas en formato texto */
    end

    // Señal de reloj con periodo de 20ns (f=50Hz)
    always
        #10 clk = ~clk;

    // Entradas
    initial begin
        @(negedge clk)
        cl = 1;                // puesta a cero
        @(negedge clk)
        cl = 0;                // contamos hacia arriba
        en = 1;                // durante 20 ciclos
        repeat(20) @(negedge clk);
        ud = 1;                // contamos hacia abajo 8 ciclos
        repeat(8) @(negedge clk);
        en = 0;                // puesta a cero
        cl = 1;
        @(negedge clk);
        cl = 0;
        repeat(2) @(negedge clk);
        $finish;
    end

endmodule // test


/*
   EJERCICIOS

   1. Compila y simula los ejemplos con:

        $ iverilog counter.v counter_tb.v
        $ vvp a.out

   2. Visualiza los resultados y comprueba la salida con:

        $ gtkwave counter_tb.vcd

   3. Modifica el diseño para incorporar una señal 'load' que cargue un estado
      de cuenta de una entrada 'x'. La operación de carga debe tener más
      prioridad que las operaciones de cuenta pero menos que la puesta a cero.

   4. Modifique el diseño original para obtener un contador módulo 10
      (de 0 a 9). Debes tener cuidado con la señal de fin de cuenta.
*/
