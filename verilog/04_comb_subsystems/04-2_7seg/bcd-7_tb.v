// Diseño:      bcd-7
// Archivo:     bcd-7_tb.v
// Descripción: Convertidor BCD a 7 segmentos. Banco de pruebas.
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       28/10/2010 (versión inicial)

`timescale 1ns / 1ps

// Base de tiempo para facilitar la simulación
`define BTIME 10

module test();

	// Entradas
	reg [3:0] d;

	// Salida
	wire [0:6] seg;

	// Instancia de la UUT
	sseg uut (.d(d), .seg(seg));

	// Inicialización y control de la simulación
	initial begin
		// 'd' se inicializa a '0'
		d = 0;

		// Generación de formas de onda
		// $dumpfile("test.vcd");
		// $dumpvars(0,test);

		// Impresión de resultados
		$display("d\tseg");
		$monitor("%d\t%b", d, seg);

		// La simulación finaliza tras 16 ciclos
		#(16*`BTIME) $finish;
	end

	// Generación de valores de entrada
	always
		#(`BTIME) d = d + 1;

endmodule // test

/*
   EJERCICIO

   1. Compila el diseño y el banco de pruebas con:

        $ iverilog bcd-7.v bcd-7_tb.v

   2. Simula el diseño con:

        $ vvp a.out

   3. Comprueba que el resultado se imprime en el terminal o consola del
      simulador y que el valor de 'seg' se corresponde con la entrada 'd'.
*/