// Diseño:      bcd-7
// Archivo:     bcd-7.v
// Descripción: Convertidor BCD a 7 segmentos
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       27/11/2009 (versión inicial)

/*
   Lección 4.2: Convertidor de BCD a 7 segmentos.

   Los convertidores de código BCD a 7 segmentos toman una cifra BCD expresada
   con cutro bits y generan un código de 7 bits que, al ser aplicado a un
   visualizador de 7 segmentos, muestra una representación de la cifra. Los
   visualizadores de 7 segmentos construyen los números mediante la activación
   o no de segmentos dispuestos como se muestra más abajo. Según el
   dispositivo, la activación de cada segmento se produce en nivel alto (
   aplicando un 1) o en nivel bajo (aplicando un 0).

         0
        ---                               ---
     5 |   | 1                               |
        --- 6       Ej: número 3 --->     ---
     4 |   | 2                               |
        ---                               ---
         3

   Por ejemplo, para representar el número 3 en un visualizador activo en
   nivel alto se aplicaría el código 1111001, mientras que en uno activo en
   nivel bajo se aplicaría el código 0000110.
*/

`timescale 1ns / 1ps

// Convertidor BCD a 7 segmentos con salidas activas en nivel bajo

module sseg (
    input wire [3:0] d,     // entrada BCD
    output reg [0:6] seg    // salida 7-segmentos
    );

	always @*
		case (d)
			4'h0: seg = 7'b0000001;
			4'h1: seg = 7'b1001111;
			/* completa el diseño */
			default: seg = 7'b1111110;	// la entrada no es BCD
		endcase
endmodule // sseg

/*
   EJERCICIO

   Completa la descripción del convertidor BCD a 7 segmentos y comprueba que
   la sintaxis es correcta con:

        $ iverilog bcd-7.v

   (Este ejemplo continúa en el archivo bcd-7_tb.v)
*/
