// Diseño: 	    gray
// Archivo:     gray.v
// Descripción: Convertidores bin/gray parametrizados.
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       12/11/2011 (versión inicial)

/*
   Lección 4.3: Convertidores de binario a Gray y de Gray a binario
                parametrizados.

   Los convertidores de código son bloques muy populares en los circuitos 
   digitales. El código Gray es muy útil para codificar la posición de 
   sistemas mecánicos y otros tipos de variables que pueden variar 
   continuamente, como la presión, la temperatura, etc. Como los valores
   adyacentes en código Gray sólo se diferencia en un bit, su uso evita
   lecturas incorrectas debido a que bits diferentes cambien en instantes
   ligeramente diferentes. Abajo se muestran los códigos binario natural y
   Gray para valores de 3 bits:

   N    bin     Gray
   -----------------
   0    000     000
   1    001     001
   2    010     011
   3    011     010
   4    100     110
   5    101     111
   6    110     101
   7    111     100

   Se dice que el código Gray es un código "reflejado" porque el código para
   2 bits se puede obtener escribiendo el código de 1 bit en orden inverso
   (reflejado) y añadiendo un '1' a la segunda mitad de los códigos. Y la
   misma operación puede hacerse para obtener la tabla del código de 'n' bits
   a partir de la de 'n-1' bits.

   Para poder hacer uso del código Gray necesitamos circuitos convertidores de 
   Gray a binario y de binario a Gray. Estos convertidores se pueden 
   implementar fácilmente usando puertas XOR (OR eXclusivas). Visita
   http://www.wisc-online.com/Objects/ViewObject.aspx?ID=IAU8307
   para ver una interesante explicación de los fundamentos de la conversión
   bin/Gray.

   En esta lección implementaremos estos convertidores de una forma 
   inteligente. Usaremos expresiones muy compactas que pueden describirse
   con un simple 'assign', y definiremos el número de bits de las señales
   como parámetros de forma que podremos instanciar convertidores de cualquier
   anchura de datos.
*/

`timescale 1ns / 1ps

// Convertidor bin/Gray

module bin_to_gray #(
    parameter n = 4
    )(
    input wire [n-1:0] x,
    output wire [n-1:0] z
    );

    /* Comparando las tablas de los códigos binario y Gray podemos ver que
     * cada bit de 'z' puede calcularse haciendo la operación XOR del 
     * correspondiente bit de 'x' con el siguiente bit de 'x', excepto para
     * el bit de más a la izquierda, que es el mismo:
     * 
     *    z[i] = x[i] ^ x[i+1]; z[n-1] = x[n]
     *
     * Podemos hacer esto para todos los bits de 'z' con un bucle, pero 
     * usando el operador de desplazamiento de forma inteligente podemos
     * obtener todos los bits de 'z' con una sola asignación. */
    assign z = x ^ (x >> 1);

endmodule	// bin_to_gray


// Gray/bin converter

module gray_to_bin #(
    parameter n = 4
    )(
    input wire [n-1:0] x,
    output wire [n-1:0] z
    );

    /* Comparando las tablas de los códigos binario y Gray podemos ver que
     * cada bit de 'z' puede calcularse haciendo la operación XOR del 
     * correspondiente bit de 'x' con el siguiente bit de 'z', excepto para
     * el bit de más a la izquierda, que es el mismo:
     * 
     *    z[i] = x[i] ^ z[i+1]; z[n-1] = x[n]
     *
     * Podemos hacer esto para todos los bits de 'z' con un bucle, pero 
     * usando el operador de desplazamiento de forma inteligente podemos
     * obtener todos los bits de 'z' con una sola asignación. */
    assign z = x ^ (z >> 1);

endmodule	// gray_to_bin

/*
   EJERCICIO

   Comprueba que la sintaxis es correcta compilando el diseño con:

      $ iverilog gray.v

   Este ejemplo continúa en el archivo 'gray_tb.v'.
*/
