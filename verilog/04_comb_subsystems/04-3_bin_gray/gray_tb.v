// Diseño: 	    gray
// Archivo:     gray.v
// Descripción: Convertidores bin/gray parametrizados. Banco de pruebas.
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       12/11/2011 (versión inicial)

`timescale 1ns / 1ps

// Tiempo base para facilitar el control de la simulación
`define BTIME 10

module test();

	// Entrada
	reg [3:0] x;

	// Salidas
	wire [3:0] z_gray, z_bin;

	// Circuitos bajo test: conexión en cascada de bin_to_gray y
	// gray_to_bin.
	bin_to_gray bin_to_gray1 (.x(x), .z(z_gray));
	gray_to_bin gray_to_bin1 (.x(z_gray), .z(z_bin));

	// Inicialización de entradas y control de la simulación
	initial begin
		// Inicialmente, todas las entradas son cero
		x = 4'b0000;

		// Generación de formas de onda
		/* $dumpfile("gray_tb.vcd"); */
		/* $dumpvars(0,test); */

		// Impresión de las salidas
		/* 'z_bin' debe ser igual a 'x' si los convertidores están
		 * funcionando correctamente. */
		$display("     ----------  z_gray  ----------         ");
		$display("x --| bin/gray |--------| gray/bin |-- z_bin");
		$display("     ----------          ----------         ");
		$display(" ");
		$display("x\tz_gray\tz_bin ");
		$display("---------------------");
		$monitor("%b\t%b\t%b", x, z_gray, z_bin);

		// Fin de la simulación
		/* Aplicamos un nuevo patrón de simulación cada BTIME ns. Calculamos
		 * el tiempo de fin de la simulación en función de BTIME para
		 * simular 16 patrones (todas las posibilidades). */
		#(16*`BTIME) $finish;
	end

	// Generación de los patrones de entrada
	/* Basta con incrementar 'x' para generar todos los posibles valores
	 * en la señal de entrada. */
	always
		#(`BTIME) x = x + 1;

endmodule // test

/*
   EJERCICIO

   1. Compila el banco de pruebas con:

        $ iverilog gray.v gray_tb.v

   2. Simula el disñeo con:

        $ vvp a.out

   3. Modifica el banco de pruebas para simular convertidores bin/gray y
      gray/bin de 8 bits. Comprueba los resultados.
*/
