// Diseño:      analysis
// Archivo:     analysis.v
// Descripción: Ejemplos de análisis de circuitos en Verilog
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       27/11/2009 (versión inicial)

/*
   Lección 4.4: Análisis de un subsistema combinacional.

   En esta unidad hemos visto como describir módulos combinacionales que
   podemos reutilizar en otros diseños, como el convertidor de BCD a 7
   segmentos o los convertidores de código binario y Gray. Aunque también
   hemos visto como describir otros bloques básicos (multiplexores,
   decodificadores, comparadores, etc.) no suele ser necesario definir estos
   bloque explícitamente para luego interconectarlos, sino que es más fácil,
   rápido y, a menudo, más eficiente describir el comportamiento que queremos
   y dejar que las herramientas de síntesis elijan e interconecten los
   componentes adecuados. Por ejemplo, el siguiente código activa dos señales
   en función del resultado de una comparación:

        always @(*)
            if (a < b) begin
                y = 1;
                z = 0;
            end else begin
                y = 0;
                z = 1;
            end
    
   Es sencillo interpretar lo que hace el código y no es necesario conocer
   en detalle qué componentes usar y cómo conectarlos para hacer el circuito,
   pero si es muy útil para el diseñador comprender que "a < b" requerirá el
   uso de un comparador y que el bloque "if" se resolverá probablemente con
   algún tipo de multiplexor o decodificador. 

   Esta lección propone un ejercicio para practicar la definición de módulos
   combinacionales, su interconexión y el diseño de implementaciones 
   alternativas.

   Se quiere diseñar el circuito de la figura en Verilog y simúlarlo para
   obtener la tabla de verdad de la función f(x,y,z) que implementa. Para ello,
   lo más directo es describir cada componente como un módulo Verilog (excepto 
   la puerta NAND que es una primitiva de Verilog) y hacer una descripción 
   estructural (interconexión de módulos) del circuito completo, como en el
   esquema de código más abajo.

       +---------+       +---+
       |DEC2:4  0|o------| & |
       |         |       |   |o------+
   y---|1       1|o------|   |    +--+-+
       |         |       +---+    |  E  \
   z---|0       2|o---------------|0     \__ f
       |         |                |      /
       |        3|o---------------|1    /
       +---------+                +--+-+
                                     |
                                     x
*/

module dec4 (
    input  wire [1:0] in,
    output reg  [3:0] out
    );

    /* La implementación del decodificador va aquí */

endmodule // dec4

module mux2 (
    input  wire [1:0] in,
    input  wire       sel,
    input  wire       en,
    output reg        out
    );

    /* La implementación del multiplexor va aquí */

endmodule // mux2

module system (
    input  wire x,
    input  wire y,
    input  wire z,
    output wire f
    );

    /* La declaración de señales de interconexión va aquí */

    // Instanciación y conexión de módulos
    dec4 dec4_1(/* completar conexión de señales */);
    nand nand_1(/* completar conexión de señales */);
    mux2 mux2_1(/* completar conexión de señales */);

endmodule // system

/*
   EJERCICIO

   1. Completa la descripción estructural del circuito propuesto.

   2. Escribe el banco de pruebas en un archivo aparte 'analisys_tb.v'.
      El banco de pruebas debe obtener la tabla de verdad completa de la
      función que implementa el circuito. Por ejemplo, recorriendo todos los
      posibles valores de las entradas e imprimiendo cada valor de entrada y
      la salida correspondiente. Utiliza los bancos de prueba de los otros
      ejemplos de la unidad como referencia.

   3. Realiza una descripción alternativa de 'system' de forma procedimental.
      Comprueba que la nueva descripción produce las mismas salidas que la
      versión original empleando el banco de pruebas desarrollado.

   4. Realiza una descripción alternativa de 'system' de forma funcional.
      Comprueba que la nueva descripción produce las mismas salidas que las
      versiones anteriores empleando el banco de pruebas desarrollado.
*/