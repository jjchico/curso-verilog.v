// Diseño:      combinational_subsystems
// Archivo:     combinational_subsystems.v
// Descripción: Ejemplos de subsistemas combinacionales en Verilog
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       27/11/2009

/*
   Unidad 4: Subsistemas combinacionales

   Las herramientas de síntesis automática de circuitos digitales emplean
   algoritmos para inferir los elementos de circuito más adecuados
   (decodificadores, multiplexores, comparadores, etc.) a la hora de
   implementar la funcionalidad descrita en un lenguaje de descripción de
   hardware. Para que el proceso sea eficiente, el diseñador debe poner
   especial atención en hacer descripciones simples y compatibles con los
   algoritmos de síntesis. Las descripciones tienen estas características se
   denominan "descripciones sintetizables".

   Se pueden enumerar multitud de criterios y recomendaciones con objeto de que
   una descripción sea sintetizable, pero todos ellos pueden resumirse en la
   siguiente frase:

     Si el diseñador no es capaz de intuir cómo hacer la síntesis de una
     determinada descripción, la herramienta de síntesis, probablemente,
     tampoco.

   Durante el proceso de síntesis, las herramientas informan de los elementos
   que van a emplear para implementar una descripción. Es un buen ejercicio
   consultar esta información y comprobar si somos capaces de darle sentido.

   Esta unidad consta de cuatro lecciones:

   Lección 4.1 (subsystems.v): contiene ejemplos de diseño de diferentes
   subsistemas y un banco de pruebas para cada uno de ellos.

   Lección 4.2 (bcd-7.v): contiene la especificación de un convertidor de
   código BCD a 7 segmentos, el cual debe diseñarse como ejercicio.

   Lección 4.3 (gray.v): diseño de convertidores binario a Gray y Gray a
   binario parametrizables.

   Lección 4.4 (analisys.v): contiene un ejercicio en el que se debe describir
   y simular en Verilog un circuito suministrado como esquema.
*/

module combinational_subsystems ();

    initial
        $display("Subsistemas combinacionales.");

endmodule