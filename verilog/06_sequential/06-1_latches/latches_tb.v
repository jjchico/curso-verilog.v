// Diseño:      biestables
// Archivo:     latches_tb.v
// Descripción: Ejemplos de biestables
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       31/05/2010 (initial version)

/*
   Lección 6.1: Biestables

   Banco de pruebas para latches.v. En este archivo se describe un banco de
   pruebas que permite comparar el comportamiento de los diversos biestables
   SR descritos, permitiendo observar las diferencias entre los distintos tipos
   de condiciones de disparo.
*/

`timescale 1ns / 1ps

// Banco de pruebas

module test ();

    reg clk = 0;            // reloj
    reg s = 0;              // entrada puesta a 1
    reg r = 0;              // entrada puesta a 0
    wire qa, qg, qms, qff;  // salidas de cada tipo de biestable

    // Instanciación de biestables
    /* Biestable asíncrono */
    sra uut_sra(.s(s), .r(r), .q(qa));
    /* Biestable disparado por nivel alto */
    srg uut_srg(.clk(clk), .s(s), .r(r), .q(qg));
    /* Biestable maestro-esclavo */
    srms uut_srms(.clk(clk), .s(s), .r(r), .q(qms));
    /* Biestable disparado por flanco de bajada */
    srff uut_srff(.clk(clk), .s(s), .r(r), .q(qff));

    // Salidas y control de la simulación
    initial begin
        // Generamos formas de onda para visualización posterior
        $dumpfile("latches_tb.vcd");
        $dumpvars(0, test);

        // Generamos salida en formato texto
        $display("      time  clk s r   qa  qg  qms qff");
        $monitor("%d  %b   %b %b   %b   %b   %b   %b",
                   $stime, clk, s, r, qa, qg, qms, qff);

        // Entradas de control
        /* Realizamos cambios en 's' y 'r' para producir distintos
         * cambios de estado. Los retrasos son relativos a la
         * asignación anterior. A la derecha se indica el tiempo
         * absoluto de cada cambio de señal. */
        #8  r = 1;    // t = 8
        #17 r = 0;    // t = 25
        #9  s = 1;    // t = 34
        #2  s = 0;    // t = 36
        #8  r = 1;    // t = 44
        #2  r = 0;    // t = 46
        #6  s = 1;    // t = 52

        // Finalizamos la simulación
        #20 $finish;
    end

    // Señal de reloj periódica (periodo de 20ns)
    always
        #10 clk = ~clk;

endmodule // test

/*
   EJERCICIOS

   1. Compila y simula los ejemplos con:

        $ iverilog latches.v latches_tb.v
        $ vvp a.out

   2. Visualiza los resultados y compara las salidas de cada tipo de biestable
      con:

        $ gtkwave test.vcd

   3. Modifica el diseño para simular:
      - Un biestable SR asíncrono con entradas activas en nivel bajo.
      - Un biestable SR activo en nivel bajo
      - Un biestable SR maestro-esclavo activo en nivel bajo
      - Un biestable SR activo en flanco de subida
*/
