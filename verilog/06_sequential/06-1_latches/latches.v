// Diseño:      biestables
// Archivo:     latches.v
// Descripción: Ejemplos de biestables
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       31/05/2010 (versión inicial)

/*
   Lección 6.1: Biestables

   Los biestables son elementos de memoria simples que pueden almacenar un
   bit (0 ó 1) y son la base de los circuitos secuenciales. El valor almacenado
   se puede cambiar mediante las entradas de excitación del biestable. En la
   mayoría de los casos prácticos el cambio de estado es controlado por una
   señal especial (señal de reloj).

   En esta lección se realizan descripciones de biestables SR con distintos
   tipos de disparo (formas de actuación de la señal de reloj) con objeto de
   comparar sus comportamientos.
*/

`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////
// Biestable SR asíncrono                                               //
//////////////////////////////////////////////////////////////////////////
/*
   Este tipo de biestable no dispone de señal de reloj. Cuando se activa la
   señal 's' (s = 1) el estado del biestable cambia a '1', y cuando se activa la
   señal 'r' (r = 1) el estado cambia a '0'. Cuando ninguna entrada está activa
   el biestable conserva su valor. La operación del biestable SR con ambas
   entradas activas no está definida por lo que hacemos que el estado cambie
   al valor desconocido ('x') en nuestra descripción para representar que
   se ha producido una combinación prohibida de las entradas.
 */

module sra (
    input wire s,    // puesta a '1'
    input wire r,    // puesta a '0'
    output reg q     // estado (valor almacenado)
    /* Las variables (tipo 'reg' en Verilog) son el único tipo de señal que
     *  puedenretener su valor y por tanto deben usarse para describir
     * comportamiento secuencial (con memoria). */
    );

    always @(s, r)
        case ({s, r})
        /* La asignación de elementos de memoria debe realizarse
         * mediante el operador de asignación no bloqueante '<=' en vez
         * del operador bloqueante normal '='. Con '<=', todas las expresiones
         * de la parte derecha de las asignaciones en un bloque de código se
         * evalúan en primer lugar pero la asignaciones sólo se producen al
         * final del bloque. Este comportamiento modela mejor el funcionamiento
         * de los circuitos secuenciales. Más detalles sobre las diferencias
         * entre el operador '=' y el '<=' se explican en la Lección 6.3. */
        2'b01: q <= 1'b0;
        2'b10: q <= 1'b1;
        2'b11: q <= 1'bx;

        /* El comportamiento secuencial del circuito se deriva de que
         * la señal 'q' no se asigna en todos los casos. Cuando 's' y
         * 'r' son ambos cero no se asigna ningún valor a 'q' por lo que
         * conserva su valor anterior, modelando el comportamiento de
         * un elemento de memoria. */
        endcase
endmodule // sra

//////////////////////////////////////////////////////////////////////////
// Biestable SR disparado por nivel alto                                //
//////////////////////////////////////////////////////////////////////////
/*
   La diferencia de este biestable es que el cambio de estado está controlado
   por la señal de reloj 'clk'. El cambio de estado sólo se produce si clk=1
   (nivel alto). De forma similar, los biestables que cambian su estado
   sólo cuando clk = 0 se dice que son activados en nivel bajo.

   Los biestables controlados por una señal de reloj se denominan en general
   "biestables síncronos".
*/

module srg (
    input wire clk, // reloj
    input wire s,   // puesta a '1'
    input wire r,   // puesta a '0'
    output reg q    // estado
    );

    always @(clk, s, r)
        if (clk == 1)
            case ({s, r})
            2'b01: q <= 1'b0;
            2'b10: q <= 1'b1;
            2'b11: q <= 1'bx;
        endcase
endmodule // srg


//////////////////////////////////////////////////////////////////////////
// Biestable SR maestro-esclavo                                         //
//////////////////////////////////////////////////////////////////////////
/*
   Este tipo de biestables se construye conectando en cadena dos biestables
   disparados por niveles diferentes de la señal de reloj. Con clk=1, el primer
   biestable (maestro/master) carga un nuevo estado mientras que el segundo
   (esclavo/slave) retiene el estado anterior. Cuando clk=0, el maestro retiene
   el estado almacenado y se transfiere al esclavo. El efecto es similar al
   del biestable disparado por nivel, salvo que el nuevo estado sólo aparece
   a la salida tras el flanco de bajada de la señal de reloj. 
   
   El motivo de usar la estructura M-S (master-slave) es hacer que la salida
   del dispositivo pueda cambiar sólo cuando el reloj cambia de '1' a '0'
   (o de '0' a '1' si los biestables son activos en nivel bajo). Al hacer
   que la salida sólo cambien en un momento muy preciso (el flanco de bajada
   de la señal de reloj) permite que los circuitos sean más robustos. Sin
   embargo, el próximo estado vendrá determinado por el valor de 's' y 'r'
   durante todo el tiempo en que la señal de reloj esté activa, como en
   el biestable SR activado por nivel. */

module srms (
    input wire clk, // reloj
    input wire s,   // puesta a '1'
    input wire r,   // puesta a '0'
    output wire q   // estado
    );

    srg master(.clk(clk), .s(s), .r(r), .q(qm));
    /* Las entradas del esclavo se conectan de forma que copien el estado
     * del maestro. */
    srg slave(.clk(clk_neg), .s(qm), .r(qm_neg), .q(q));

    /* Generamos la salida complementada del maestro y la señal de reloj
     * invertida para el esclavo. */
    assign qm_neg = ~qm;
    assign clk_neg = ~clk;

endmodule // srms


//////////////////////////////////////////////////////////////////////////
// Biestable SR disparado por flanco de bajada                          //
//////////////////////////////////////////////////////////////////////////
/*
   Los biestables disparados por flanco cambián de estado en función de las
   entradas presentes en el momento del cambio de la señal de reloj, bien sea
   de 0 a 1 (flanco de subida) o de 1 a 0 (flanco de bajada). Proporcionan un
   mejor control de los cambios de estado por lo que se usan preferentemente
   en los circuitos secuenciales.
*/

module srff (
    input wire clk, // reloj
    input wire s,   // puesta a '1'
    input wire r,   // puesta a '0'
    output reg q    // estado
    );

    /* La condición de disparo por flanco se indica mediante las directivas
     * 'posedge' y 'negedge' en las listas de sensibilidad de los
     * procesos. En este caso, el proceso será evaluado únicamente en los
     * cambios de 1 a 0 de la señal de reloj y no ante cualquier cambio.
     * Los cambios o valores de las entradas de control en otros instantes
     * no afectan porque no se han incluído en la lista de sensibilidad. */
    always @(negedge clk)
        case ({s, r})
        2'b01: q <= 1'b0;
        2'b10: q <= 1'b1;
        2'b11: q <= 1'bx;
        endcase
endmodule // srff

/*
   (La lección continúa en 'latches_tb.v').
*/
