// Diseño:      arbitrador
// Archivo:     arbiter_tb.v
// Descripción: Arbitrador. Ejemplo de máquina de estados
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       04/06/2010 (versión inicial)

/*
   Lección 6.5: Máquinas de estados finitos. Arbitrador

   Este archivo contine un banco de pruebas para los módulos en 'arbiter.v'.
*/

`timescale 1ns / 1ps

// Banco de pruebas

module test ();

    reg clk = 0;  // reloj
    reg r1 = 0;   // petición 1
    reg r2 = 0;   // petición 2
    wire g1;      // concesión 1
    wire g2;      // concesión 2

    // Circuito bajo test
    arbiter1 uut(.clk(clk), .r1(r1), .r2(r2), .g1(g1), .g2(g2));

    // Generación de ondas y control de fin de simulación
    initial begin
        $dumpfile("arbiter_tb.vcd");
        $dumpvars(0, test);

        #240 $finish;
    end

    // Señal de reloj
    always
        #10 clk = ~clk;

    // Peticiones del dispositivo 1
    /* Las señales de petición de cada dispositivo se generan por
     * separado. En este caso se han empleado bloque "fork-join" para
     * que las referencias de los tiempos en las asignaciones sean
     * absolutas al comienzo del proceso, como vimos en la lección 6.2. */
    initial fork
        #20  r1 = 1;
        #40  r1 = 0;
        #100 r1 = 1;
        #120 r1 = 0;
        #180 r1 = 1;
        #200 r1 = 0;
    join

    // Peticiones de dispositivo 2
    initial fork
        #60  r2 = 1;
        #80  r2 = 0;
        #110 r2 = 1;
        #140 r2 = 0;
        #180 r2 = 1;
        #220 r2 = 0;
    join
endmodule

/*
   EJERCICIOS

   1. Simula el diseño y comprueba su operación. Para ello visualiza las
      entradas y salidas del módulo y su estado interno ('state').

   2. Detecta el error en el cálculo de la salida del módulo 'arbiter1',
      corrígelo y vuelve a simular el diseño para comprobar.

   3. Sustituya en el banco de pruebas el módulo 'arbiter1' por 'arbiter2',
      simúlalo y observa el resultado. ¿Es correcto?

   4. Compare los resultados de los módulos 'arbiter1' y 'arbiter2'. Para
      hacerlo, puedes modificar el banco de pruebas para que muestra las
      salidas de ambos arbitradores o simularlos por turnos.

      a) ¿Son los resultados idénticos?
      b) ¿Son equivalentes?
      c) Reflexiona sobre que implementación te parece más adecuada para
         este diseño, Mealy (arbiter1) o Moore (arbiter2).
*/
