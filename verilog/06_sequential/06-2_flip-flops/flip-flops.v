// Diseño:      flip-flops
// Archivo:     flip-flops.v
// Descripción: Ejemplos de biestables disparados por flanco (flip-flops)
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       19/06/2010 (versión inicial)

/*
   Lección 6.2: Flip-flops

   En esta lección se realizan descripciones de flip-flops SR, JK, D y T con
   entradas de puesta a cero y a uno asíncronas. Todos los FF están disparados
   por flanco de subida. Las entradas asíncronas son activas en nivel bajo.

   Las entradas asíncronas en los flip-flops, cuando están presentes, tienen
   prioridad sobre las entradas síncronas controladas por el reloj. Esto
   significa, por ejemplo, que si la señal de puesta a cero asíncrona está
   activa, el flip-flop permanecerá en estado '0' sin importar que valor
   tengan las entradas síncronas o qué eventos ocurran en el reloj.
*/

`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////
// Flip-flop SR                                                         //
//////////////////////////////////////////////////////////////////////////

module srff (
    input wire clk, // reloj
    input wire s,   // entrada 's'
    input wire r,   // entrada 'r'
    input wire cl,  // puesta a '0' asíncrona
    input wire pr,  // puesta a '1' asíncrona
    output reg q    // estado
    );

    always @(posedge clk, negedge cl, negedge pr)
        if (!cl)
            q <= 1'b0;
        else if (!pr)
            q <= 1'b1;
        else
            case ({s, r})
            2'b01: q <= 1'b0;
            2'b10: q <= 1'b1;
            2'b11: q <= 1'bx;
            endcase
endmodule // srff

//////////////////////////////////////////////////////////////////////////
// Flip-flop JK                                                         //
//////////////////////////////////////////////////////////////////////////

module jkff (
    input wire clk, // reloj
    input wire j,   // entrada 'j'
    input wire k,   // entrada 'r'
    input wire cl,  // puesta a '0' asíncrona
    input wire pr,  // puesta a '1' asíncrona
    output reg q    // estado
    );

    always @(posedge clk, negedge cl, negedge pr)
        if (!cl)
            q <= 1'b0;
        else if (!pr)
            q <= 1'b1;
        else
            case ({j, k})
            2'b01: q <= 1'b0;
            2'b10: q <= 1'b1;
            2'b11: q <= ~q;
            endcase
endmodule // jkff

//////////////////////////////////////////////////////////////////////////
// Flip-flop D                                                          //
//////////////////////////////////////////////////////////////////////////

module dff (
    input wire clk, // reloj
    input wire d,   // entrada 'd'
    input wire cl,  // puesta a '0' asíncrona
    input wire pr,  // puesta a '1' asíncrona
    output reg q    // estado
    );

    always @(posedge clk, negedge cl, negedge pr)
        if (!cl)
            q <= 1'b0;
        else if (!pr)
            q <= 1'b1;
        else
            q <= d;
endmodule // dff

//////////////////////////////////////////////////////////////////////////
// Flip-flop T                                                          //
//////////////////////////////////////////////////////////////////////////

module tff (
    input wire clk, // reloj
    input wire t,   // entrada 't'
    input wire cl,  // puesta a '0' asíncrona
    input wire pr,  // puesta a '1' asíncrona
    output reg q    // estado
    );

    always @(posedge clk, negedge cl, negedge pr)
        if (!cl)
            q <= 1'b0;
        else if (!pr)
            q <= 1'b1;
        else if (t)
            q <= ~q;
endmodule // tff

/*
   (Esta lección continúa en 'flip-flops_tb.v'.)
*/
