// Diseño:      flip-flops
// Archivo:     flip-flops_tb.v
// Descripción: Ejemplos de biestables disparados por flanco (flip-flops)
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       31/05/2010 (versión inicial)

/*
   Lección 6.2: Flip-flops

   Banco de pruebas para flip-flops.v. En este archivo se describe un banco de
   pruebas que permite comparar el comportamiento de los diversos biestables
   disparados por flanco (flip-flops) descritos en flip-flops.v, permitiendo
   observar las diferencias entre los distintos tipos de dispositivo.
*/

`timescale 1ns / 1ps

// Banco de pruebas

module test ();

    reg clk = 0;    // reloj
    reg j = 0;      // entrada puesta a 1
    reg k = 0;      // entrada puesta a 0
    reg d = 0;      // entrada de dato
    wire s;         // entrada puesta a 1
    wire r;         // entrada puesta a 0
    wire t;         // entrada de conmutación (toggle)
    reg cl = 1;     // puesta a 0  asíncrona
    reg pr = 1;     // puesta a 1 asíncrona
    wire qsr, qjk, qd, qt;    // salidas de cada tipo de biestable

    // Algunas entradas son compartidas
    assign s = j;
    assign r = k;
    assign t = d;

    // Instanciación de biestables
    srff uut_srff(.clk(clk), .s(s), .r(r), .cl(cl), .pr(pr), .q(qsr));
    jkff uut_jkff(.clk(clk), .j(j), .k(k), .cl(cl), .pr(pr), .q(qjk));
    dff uut_dff(.clk(clk), .d(d), .cl(cl), .pr(pr), .q(qd));
    tff uut_tff(.clk(clk), .t(t), .cl(cl), .pr(pr), .q(qt));

    // Control de la simulación
    initial begin
        // Generamos formas de onda para visualización posterior
        $dumpfile("flip-flops_tb.vcd");
        $dumpvars(0, test);

        // Fin de la simulación
        #180 $finish;
    end

    // Clear y preset asíncronos
    /* Los bloques delimitados por 'fork-join' son similares a los 'begin-end'
     * pero todas las directivas que contienen se ejecutan de forma concurrente.
     * Por lo tanto, los retrasos indicados en cada directiva son relativos
     * al comienzo del proceso y no al retraso anterior. */
    initial fork
        #5   cl = 0;
        #20  cl = 1;
        #140 pr = 0;
        #160 pr = 1;
    join

    // Entradas JK y SR (s=j, r=k)
    initial fork
        #20  j = 1;
        #40  j = 0;
        #60  k = 1;
        #80  k = 0;
        #100 j = 1;
        #100 k = 1;
        #160 j = 0;
        #160 k = 0;
    join

    // Entradas D y T
    initial fork
        #20  d = 1;
        #40  d = 0;
        #80  d = 1;
        #140 d = 0;
    join

    // Señal de reloj periódica (periodo de 20ns)
    always
        #10 clk = ~clk;

endmodule // test

/*
   EJERCICIOS

   1. Compila y simula los ejemplos con:

        $ iverilog flip-flops.v flip-flops_tb.v
        $ vvp a.out

   2. Visualiza los resultados y compara las salidas de cada tipo de biestable
      con:

        $ gtkwave flip-flops_tb.vcd

   3. Realiza descripciones de biestables JK, D y T disparados por flanco
      de bajada. Escribe un banco de pruebas adecuado para comprobar los
      diseños.
*/
