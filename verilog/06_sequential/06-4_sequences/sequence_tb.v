// Diseño:      seq
// Archivo:     sequence_tb.v
// Descripción: Ejemplo de máquinas de estados finitos (FSM). Detectores de
//              secuencia con solapamiento. Versiones Mealy y Moore.
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       09/06/2010 (versión inicial)

/*
   Lección 6.4: Máquinas de estados finitos. Detector de secuencia.

   Banco de pruebas para módulos en 'sequence.v'. El banco de pruebas
   proporciona una secuencia idéntica a las implementaciones Mealy y Moore
   del detector de secuencia con objeto de poder comparar la salida generada
   por cada tipo de máquina.
*/

// En este banco de pruebas la entrada de datos es síncrona con el reloj pero
// puede estar desfasada con respecto a su flanco activo (flanco positivo).
// Esta macro controla el valor del desplazamiento. Un valor de '1' es
// realista porque se espera que las entradas síncronas cambien justo después
// del flanco activo del reloj, pero no exactamente en el mismo instante. La
// macro XSHIFT puede cambiarse para observar el efecto en el comportamiento
// de la máquina
//     0: entrada en fase con el flanco activo del reloj.
//  1-19: entrada retrasada respecto al flanco activo del reloj.
`define XSHIFT 1

`timescale 1ns / 1ps

// Banco de pruebas

module test ();
    reg clk = 1;    // reloj
    reg reset = 0;  // reset
    reg x = 0;      // entrada
    wire z_mealy;   // salida Mealy
    wire z_moore;   // salida Moore

    /* El vector 'in' contiene una secuencia de 32 bits que se suministrará
     * a la entrada. 'n' es el número de bits de la secuencia que se
     * aplicarán e 'i' actúa como índice. */
    reg [0:31] in = 32'b00100111_00001110_10010010_01010011;
    integer n = 32;
    integer i = 0;

    // Instanciación de módulos
    seq_mealy uut_mealy (.clk(clk), .reset(reset), .x(x), .z(z_mealy));
    seq_moore uut_moore (.clk(clk), .reset(reset), .x(x), .z(z_moore));

    // Generación de ondas
    initial begin
        $dumpfile("sequence_tb.vcd");
        $dumpvars(0, test);
    end

    // Generador de reloj (20ns period)
    always
        #10 clk = ~clk;

    // Generador de secuencia de entrada
    always @(posedge clk) begin
        // cambiamos la entrada tras algo de retraso
        #`XSHIFT x = in[i];

        // Incrementamos el índice y finalizamos si se han aplicado
        // todos los valores
        i = i + 1;
        if (i == n)
            $finish;
    end

    // Reset
    /* Un proceso independiente controla la señal 'reset'. Se realiza una
     * iniciación al principio de la simulación y otra durante la
     * operación del sistema. */
    initial begin
        #5   reset = 1;
        #15  reset = 0;
        #450 reset = 1;
        #20  reset = 0;
    end
endmodule

/*
   EJERCICIOS

   1. Compila y simula el diseño con:

        $ iverilog sequence.v sequence_tb.v
        $ vvp a.out

      y visualiza los resultados con:

        $ gtkwave sequence_tb.vcd

      Represente las señales de entrada y salida y los estados internos de
      las máquinas de Mealy y Moore.

   2. Responde y reflexiona sobre las siguientes cuestiones:

      a) ¿Son idénticas las salidas de la máquina de Mealy y Moore?
      b) ¿Es correcta la operación de la máquina de Mealy?
      c) ¿Es correcta la operación de la máquina de Moore?
      d) Explica las diferencias que se aprecian en las salidas de ambas
         máquinas.

   3. Prueba a repetir la simulación asignando
      valores a XSHIFT entre 0 y 19. Observa la salida de ambas máquinas.

      a) ¿La salida de qué máquina se ve más afectada por la no sincronización
         entre el cambio de la señal de reloj y la entrada?
      b) ¿Qué tipo de máquina crees que será más robusta frente a la presencia
         de entradas no sincronizadas?

   4. Cambia la secuencia de entrada en 'in' y comprueba la operación de
      ambas máquinas con la nueva secuencia.

   5. ¿Qué pasaría si se borra (o se comenta) el bloque que controla la señal
      'reset'? Deduce el resultado usando la tabla de estados de la máquina y
      comprueba el resultado con la simulación. ¿Has acertado?
*/
