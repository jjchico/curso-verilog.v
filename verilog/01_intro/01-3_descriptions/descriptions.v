// Diseño:      descripciones
// Archivo:     descriptions.v
// Descripción: Tipos de descripciones en Verilog
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       05-11-2009 (versión inicial)

/*
   Lección 1.3: Tipos de descripciones en Verilog

   En esta lección veremos las tres formas de describir un circuito en Verilog.
   Estas son:

   - Descripción funcional: el comportamiento del circuito se describe
     mediante expresiones lógicas, tal como vimos en la lección 2.

   - Descripción procedimiental: se emplea un procedimiento con estructuras de
     control (if-else, while, case, etc.) de forma similar a los lenguajes de
     programación.

   - Descripción estructural: consiste en especificar la interconexión de
     componentes previamente diseñados (módulos) o de primitivas lógicas (AND,
     OR, NOT, etc.).

   De todas ellas, la descripción procedimental es la de más alto nivel y la
   que permite describir circuitos más facilmente. La funcional es equivalente
   al empleo de expresiones lógicas y se emplea en asignaciones permanetes
   (assign) y en procedimientos en combinación con una descripción
   procedimental. La descripción estructural es la más cercana a una
   implementación real del circuito. Se suele emplear para definir la forma
   de interconexión de grandes módulos del sistema, empleando para cada módulo
   una forma de descripción más adecuada.

   En esta lección realizaremos tres versiones de la función
     f = a & ~b | ~a & b
   empleando los tres tipos de descripciones.
*/

/* Como en la lección anterior, fijamos la escala de tiempos */
`timescale 1ns / 1ps

/* Empleamos un único módulo sin conexiones externas ya que tanto la
 * funcionalidad del circuito como las señales empleadas para su simulación
 * se generan en su interior. */
module descriptions ();

    /* Usaremos a y b como variables de la función a implementar. Las
     * iniciamos a cero y las haremos variar durante la simulación. */
    reg a = 0, b = 0;

    //// Descripción funcional ////

    /* 'f_func' es el resultado de la implementación funcional. Primero se
     * declara la señal como 'wire' y luego se realiza una asignación continua
     * con 'assign' como en la lección anterior. */
    wire f_func
    assign f_func = a & ~b | ~a & b;

    /* También es posible combinar la declaración con la asignación continua,
     * por ejemplo:
     *     wire f_func = a & ~b | ~a & b;
     * Aquí el 'assign' queda implícito. */

    //// Descripción procedimental ////

    /* 'f_proc' corresponde a la implementación procedimental de la función.
     * A diferencia de las señales de tipo 'wire', que deben estar siempre
     * conectadas o ser asignadas de forma continua, las señales empleadas en
     * los procedimientos pueden asignarse varias veces o no ser asignadas.
     * Para este tipo de señales Verilog usa señales de tipo 'variable' que
     * son declaradas mediante la palabra clave 'reg'. Como regla general,
     * todas las señales asignadas en procedimiento deben ser de tipo
     * 'reg'. [1] */
    reg f_proc;

    /* Una descripción procedimental se produce siempre en un bloque
     * "always". El bloque se evalúa sólo cuando hay un cambio en alguna
     * de las señales indicadas en su lista de sensibilidad. Las señales en
     * la lista de sensibilidad se listan separadas por 'or' o ','. En
     * nuestro caso, la función se evalúa ante un cambio en cualquiera de
     * las entradas, como es propio de un circuito combinacional. */
    always @(a, b)
    /* Cuando un bloque o directiva contiene más de una expresión o
     * sentencia, éstas se agrupan con begin-end. en este caso no es
     * necesario porque el bloque "always" sólo contiene una sentencia
     * "if-else" pero se incluye por claridad. */
    begin
        /* Una sentencia "if-else" permite hacer asignaciones
         * condicionales.  en la condición se emplean operadores
         * relacionales, por ejemplo:
         *  == - igual
         *  != - no igual
         *  && - ambos ciertos
         *  || - cualquiera cierto */
        if (a == 0)
            f_proc = b;
        /* En la descripción de circuitos combinacionales deben
         * contemplarse todos los posibles valores de las señales. De
         * no ser así, las señales no asignadas conservan su valor
         * anterior y se convierten en elementos de memoria, que no son
         * combinacionales. */
        else
            f_proc = ~b;
    end

    //// Descripción estructural ////

    /* La descripción estructural consiste en la colocación e interconexión de
     * módulos diseñados previamente y/o de primitivas lógicas. Las primitivas
     * lógicas son módulos básicos reconocidos por Verilog como diversas
     * puertas lógicas (and, or, xor, nand, nor, xnor, not, buf, etc.). La
     * descripción estructural es una representación directa del esquema del
     * circuito, esto es, un dibujo de los módulos del circuito y sus
     * conexiones con otros módulos. De hecho, la forma más fácil de generar
     * una descripción estructural es partiendo de un diagrama del circuito
     * dibujado previamente en papel o en otro medio. */

    // Salida de la descripción estructural.
    /* Las señales que interconectan módulos deben ser de tipo 'wire'. */
    wire f_est;
    // Señales para la interconexión de los elementos.
    wire not_a, not_b, and1_out, and2_out;

    /* Al "instanciar" un módulo en una descripción estructural el formato es:
     *     nombre_modulo nombre_instancia (lista_de_señales)
     * En nuestro diseño vamos a usar algunas de las primitivas lógicas de
     * Verilog. Cuando se instancian primitivas lógicas, la primera señal en la
     * lista es la salida de la puerta y el resto de señales son las entradas.
     * Puede usarse un número variable de señales de entrada con puetas que
     * soportan múltiples entradas como 'and' u 'or'. La descripción
     * estructural de nuestro circuito puede ser deducida fácilmente de la
     * descripción funcional de más arriba. Ante la duda, puedes dibujar el
     * diagrama que corresponde a la siguiente descripción y compararlo con la
     * expresión de la función. */
    not not1 (not_a, a);
    not not2 (not_b, b);
    and and1 (and1_out, a, not_b);
    and and2 (and2_out, not_a, b);
    or  or1  (f_est, and1_out, and2_out);

    //// Generación de señales y directivas de test ////

    /* El resto del módulo sirve para generar las señales de test y
     * controlar la simulación */

    // Generamos señales periódicas en las entradas 'a' y 'b'
    always
        #10 a = ~a;
    always
        #20 b = ~b;

    // Iniciación y control de la simulación
    initial
    begin
        // Salvamos las formas de onda de todas las señales
        $dumpfile("descriptions.vcd");
        $dumpvars(0, descriptions);

        // Mostramos los cambios de cada señal de entrada o salida
        /* Como en la lección anterior, usamos '$monitor' para imprimir el
         * valor de todas las señales que cambian. También vamos a imprimir el
         * instante de tiempo en que se produce cada cambio. La función del
         * sistema '$timeformat' permite definir el formato para el tiempo que
         * luego imprimirá '$monitor'. Los argumentos de '$timeformat' son:
         *   -9: usamos 10^(-9) como unidades (nanosegundos)
         *    0: número de cifras decimales (ninguna)
         *   ns: sufijo a incluir detrás del número
         *    5: número máximo de cifras a imprimir ./
        $timeformat(-9, 0, "ns", 5);
        /* Usamos '$display' para imprimir una cabecera al pricipio de la
         * simulación. '\t' en la cadena de texto representa un carácter
         * tabulador. */
        $display("t\ta\tb\tf_func\tf_proc\tf_est");
        /* La cadena de formato de '$monitor' incluye un número en formato de
         * tiempo '%t' (el especificado con '$timeformat') y valores binarios
         * '%b' para las señales. La función del sistema '$stime' devuelve el
         * tiempo de simulación actual en cada acceso. */
        $monitor("%t\t%b\t%b\t%b\t%b\t%b",
                 $stime, a, b, f_func, f_proc, f_est);

        // Finalizamos la simulación a los 100ns
        #100 $finish;
    end

    /* Nota sobre la concurrencia: es importante comprender que en un lenguaje
     * de descripción de hardware como Verilog, cada procedimiento (always o
     * initial), asignación continua (assign) o instancia de módulo o
     * primitiva opera de forma concurrente con todas las demás
     * construcciones. Esto significa que podemos alterar el orden en que
     * hemos definido cada uno de estos elementos en nuestro archivo
     * Verilog sin que cambie el comportamiento del circuito modelado ni los
     * resultados de su simulación. Esta naturaleza concurrente de los LDH
     * viene dada por los elemenos que represeantan: elementos de circuitos
     * que operan de forma independiente y concurrente con el resto de
     * elementos. Los únicos elementos que se evalúan de forma secuencial
     * (como en el software) son los que se encuentran dentro de
     * procedimientos (always o initial). */

endmodule // descriptions

/*
   [1] Uno de los aspectos más confusos en Verilog para los diseñadores novatos
   es la diferencia entre los tipos 'wire' y 'reg', y también es uno de los
   aspectos de diseño de Verilog más criticados por dos motivos:
   (1) 'wire' significa 'cable' y se usa para conexiones y pero también se usa
   para asignaciones continuas.
   (2) El nombre 'reg' parece indicar que representa un 'registro', que es un
   dispositivo digital que almacena información, sin embargo, una variable
   puede representar un registro pero también puede representar señales que no
   son registros, por lo que un nombre más adecuado para este tipo de señal
   hubiera sido algo como 'var'.
   Para resolver esta confusión, SystemVerilog, una extensión del Verilog
   original, introduce el tipo 'logic' que puede usarse para cualquier señal,
   ya sea de tipo conexión o variable.
 */


/*
   EJERCICIO

   1. Compila el diseño con:

        $ iverilog descriptions.v

   2. Ejecuta la simulación con:

        $ vvp a.out

      Observa los resultados generados con $monitor para las tres
      descripciones.

   3. Visualiza las ondas generadas mediante $dumpvars con gtkwave (el "&"
      final en el comando es para dejar libre el terminal):

        $ gtkwave descriptions.vcd &

      Comprueba que el resultado de las tres implementaciones es idéntico y
      correcto.
*/
