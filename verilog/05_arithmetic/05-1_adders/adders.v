// Diseño:      sumador
// Archivo:     adders.v
// Descripción: Ejemplos de sumadores combinacionales en Verilog
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       27/11/2009 (fecha original)

/*
   Lección 5.1: Circuitos aritméticos. Sumadores.

   El sumador de números enteros es el elemento principal de las unidades
   aritméticas. Un circuito sumador puede expresarse en Verilog de forma muy
   sencilla empleando operadores aritméticos. Más tarde, una herramienta de
   síntesis adecuada producirá un circuito sumador optimizado para la tecnología
   de implementación. No obstante, un sumador puede describirse a más bajo nivel
   como interconexión de circuitos sumadores básicos. El principal sumador
   básico es el sumador completo (full adder -FA-).

   En esta lección comenzaremos con la descripción de un sumador completo que
   servirá de base para dos descripciones estructurales equivalentes de un
   sumador de 8 bits. Luego se realizará una descripción de otro sumador
   equivalente empleando operaciones aritméticas. La operación de todos ellos
   y algunas variantes se comprueba por simulación en los ejercicios propuestos.
*/

`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////
// Sumador completo (FA)                                                //
//////////////////////////////////////////////////////////////////////////

/* Un sumador completo (FA) es un sumador básico de tres bits cuyo resultado se
 * genera en un bit de suma y un bit de acarreo según la siguiente tabla
 * de verdad:
 *   a b cin s cout
 *   0 0 0   0 0
 *   0 0 1   1 0
 *   0 1 0   1 0
 *   0 1 1   0 1
 *   1 0 0   1 0
 *   1 0 1   0 1
 *   1 1 0   0 1
 *   1 1 1   1 1
 *
 * El FA está pensado para sumar cada columna en la suma de dos números
 * binarios. Realiza la suma de un bit de cada operando ('a' y 'b') junto con
 * el bit de acarreo de la columna anterior 'cin', y genera el correspondiente
 * bit 's' del resultado de la suma y el valor para ser "acarreado" a la 
 * siguiente columna, 'cout'. */
module fa (
    input wire a,       // primer operando
    input wire b,       // segundo operando
    input wire cin,     // acarreo de entrada
    output wire s,      // salida de suma
    output wire cout    // acarreo de salida
    );

    /* Las expresiones lógica para las salidas 's' y 'cout' pueden deducirse 
     * fácilmente observando que 's' vale 1 si y sólo si la paridad de las 
     * entradas es impar, y 'cout' vale 1 si y sólo si cualesquiera dos de las
     * entradas vale 1. */
    assign  s = a ^ b ^ cin;
    assign  cout = a & b | a & cin | b & cin;
endmodule // fa

//////////////////////////////////////////////////////////////////////////
// Sumador 8 bits con FA (descripción estructural)                      //
//////////////////////////////////////////////////////////////////////////

module adder8_s (
    input wire [7:0] a,     // primer operando
    input wire [7:0] b,     // segundo operando
    input wire cin,         // acarreo de entrada
    output wire [7:0] s,    // salida de suma
    output wire cout        // acarreo de salida
    );

    /* Este sumador se construye mediante la conexión en cascada de 8
     * sumadore completos (FA). Cada FA genera un bit del resultado. 'c'
     * es una señal auxiliar para la conexión del acarreo de salida de una
     * etapa con el acarreo de salida de la etapa siguiente. */
    wire [7:1] c;

    /* El acarreo de entrada del primer FA es el acarreo de entrada del
     * módulo sumador */
    fa fa0 (.a(a[0]), .b(b[0]), .cin(cin ), .s(s[0]), .cout(c[1]));
    fa fa1 (.a(a[1]), .b(b[1]), .cin(c[1]), .s(s[1]), .cout(c[2]));
    fa fa2 (.a(a[2]), .b(b[2]), .cin(c[2]), .s(s[2]), .cout(c[3]));
    fa fa3 (.a(a[3]), .b(b[3]), .cin(c[3]), .s(s[3]), .cout(c[4]));
    fa fa4 (.a(a[4]), .b(b[4]), .cin(c[4]), .s(s[4]), .cout(c[5]));
    fa fa5 (.a(a[5]), .b(b[5]), .cin(c[5]), .s(s[5]), .cout(c[6]));
    fa fa6 (.a(a[6]), .b(b[6]), .cin(c[6]), .s(s[6]), .cout(c[7]));
    /* El acarreo de salida del último FA es el acarreo de salida del
     * módulo sumador */
    fa fa7 (.a(a[7]), .b(b[7]), .cin(c[7]), .s(s[7]), .cout(cout));

endmodule // adder8_s

//////////////////////////////////////////////////////////////////////////
// Sumador 8 bits con FA usando "generate"                              //
//////////////////////////////////////////////////////////////////////////

/* Esta es una descripción equivalente a adder8_s que emplea la construcción
 * 'generate'. */
module adder8_g (
    input wire [7:0] a,     // primer operando
    input wire [7:0] b,     // segundo operando
    input wire cin,         // acarreo de entrada
    output wire [7:0] s,    // salida de suma
    output wire cout        // acarreo de salida
    );

    /* En este caso, los acarreos intermedios se definen desde 0 a 8 para
     * que todas las instancias FA puedan expresarse en función de un mismo
     * índice. El primer acarreo (C[0]) corresponde al acarreo de entrada
     * y el último (c[8]) al acarreo de salida del módulo sumador */
    wire [8:0] c;
    assign c[0] = cin;
    assign cout = c[8];

    /* Los bloques 'generate' permiten la generación automática de varias
     * estructuras Verilog en fúnción de una variable o índice, simplificando
     * la descripción de estructuras repetitivas o generando código diferente
     * dependiendo del valor de parámetros. La variable que controla la 
     * generación de código es un entero no negativo y debe declararse con
     * 'genvar' dentro o fuera del bloque 'generate'. Dentro de un bloque 
     * 'generate' pueden usarse la mayoría de las construcciones de Verilog
     * pero también pueden usarse tres tipos de estructuras de control: 'if',
     * 'case' y 'for', que no deben confundirse con los equivalentes de las 
     * descripciones procedimentales. La variable de control en los bucles 
     * 'for' debe ser una variable 'genvar'. Un bloque 'generate' se define
     * entre las palabras clave 'generate' y 'endgenerate' en Verilog 2001, pero
     * el uso de estas palabras clave es opcional a partir de Verilog 2005.
     * En nuestro módulo 'adder8_g' usamos un bucle 'for' para automatizar la
     * instanciación de 8 bloque 'fa', lo que producirá prácticamente el 
     * mismo circuito del módulo 'adder8_s' pero resulta más fácil de escribir,
     * puede expandirse fácilmente a más bits e incluso el número de bits
     * podría ser un parámetro del módulo. */
    generate
        /* Declaramos la variable de la que depende 'generate'. */
        genvar i;
        /* Bucle for asociado al bloque 'generate'. Aunque similar,
         * conceptualmente es diferente al bucle 'for' de una
         * descripción procedimental como la de la construcción
         * 'always' o 'initial'. */
        for (i=0; i < 8; i=i+1)
            /* Se genera una instancia para cada valor de 'i'. */
            fa fa_ins (.a(a[i]), .b(b[i]), .cin(c[i]), .s(s[i]), .cout(c[i+1]));
    endgenerate

endmodule // adder8_g

//////////////////////////////////////////////////////////////////////////
// Sumador de 8 bits con operadores aritméticos                         //
//////////////////////////////////////////////////////////////////////////

module adder8 (
    input wire [7:0] a,     // primer operando
    input wire [7:0] b,     // segundo operando
    input wire cin,         // acarreo de entrada
    output wire [7:0] s,    // salida de suma
    output wire cout        // acarreo de salida
    );

    /* Usamos el operador de concatenación '{...}' para almacenar el bit
     * extra de la suma directamente en 'cout'. */
    assign {cout, s} = a + b;

endmodule // adder8

/*
   EJERCICIOS

   1. Compila la lección con:

        $ iverilog adders.v

      Comprueba que no hay errores de sintáxis

   2. Escribe un banco de pruebas para el módulo 'fa' en un archivo 'fa_tb.v'
      de forma que compruebe su correcto funcionamiento para todos los valores
      de entrada. Puedes usar 'adders_tb.v' como referencia. Compila el diseño
      con:

        $ iverilog adders.v fa_tb.v

      y comprueba su operación con:

        $ vvp a.out

   (continúa en adders_tb.v)
*/
