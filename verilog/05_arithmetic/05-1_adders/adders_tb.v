// Diseño:      sumador
// Archivo:     adders_tb.v
// Descripción: Ejemplos de sumadores combinacionales en Verilog
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       23/05/2009 (versión inicial)

/*
    Lección 5.1: Circuitos aritméticos. Sumadores.

    Este archivo contiene un banco de prueba para sumadores.
*/

`timescale 1ns / 1ps

// Este banco de pruebas aplica un número de patrones de entrada aleatorios
// al circuito de pruebas.
// Macros de simulación y valores predeterminados:
//   NP: número de patrones de simulación.
//   SEED: semilla inicial para la generación de números aleatorios.

`ifndef NP
    `define NP 20   // número de patrones a aplicar en el test
`endif
`ifndef SEED
    `define SEED 1  // semilla inicial para generar patrones aleatorios
`endif

module test ();

    reg [7:0] a;           // número 'a'
    reg [7:0] b;           // número 'b'
    reg cin;               // acarreo de entrada
    wire [7:0] s;          // suma
    wire cout;             // accareo de salida
    integer np;            // variable auxiliar (número de patrones)
    integer seed = `SEED;  // variable auxiliar (semilla)

    // Circuito bajo test. Usar 'adder8_s', 'adder8_g' o 'adder8' para
    // simular las distintas versiones del sumador:
    //   adder8_s: descripción estrutural
    //   adder8_g: descripción estructural usando 'generate'
    //   adder8: descripción funcional con operandos aritméticos
    adder8_s uut(.a(a), .b(b), .cin(cin), .s(s), .cout(cout));

    initial begin
        /* 'np' se empleará como contador del número de patrones de
         * test a aplicar. Su valor inicial se carga de la macro NP. */
        np = `NP;

        // Generamos formas de onda para visualización posterior (opcional)
        $dumpfile("sumador_tb.vcd");
        $dumpvars(0, test);

        // Impresión de resultados en el terminal
        $display("A  B  cin  s  cout");
        $display("------------------");
        $monitor("%h %h %b    %h %b",
                   a, b, cin, s, cout);
    end

    // Procedimiento de generación de tests
    /* Cada 20ns 'a', 'b' y 'cin' se asignan con valores aleatorios.
     * La simulación finaliza después de aplicar un número de patrones
     * igual a NP. Pueden probarse otras secuencias pseudoaleatorias 
     * definiendo un valor diferente de SEED. Los valores de las macros NP y
     * SEED pueden cambiarse al comienzo de este archivo o mediante opciones
     * del compilador. */
    always begin
        #20
        a = $random(seed);
        b = $random(seed);
        cin = $random(seed);
        np = np - 1;
        if (np == 0)
            $finish;
    end
endmodule

/*
   EJERCICIOS

   3. Compila el banco de pruebas para el sumador con:

        $ iverilog adders.v adders_tb.v

      y comprueba su operación con:

        $ vvp a.out

      Observa la salida de texto y las formas de onda con Gtkwave y comprueba
      que son correctas. Si no lo fueran, localiza el error y corrígelo.

   4. Realiza simulaciones adicionales con distintos valores de NP y SEED.
      Por ejemplo:

        $ iverilog -DNP=40 -DSEED=2 adders.v adders_tb.v
        $ vpp a.out

   5. Modifica el banco de pruebas para simular adder8_g y adder8
      simultáneamente aplicando los mismos valores de entrada. Comprueba que
      los resultados coinciden en todos los casos. Si no es así, localiza el
      error y corrígelo.

   6. Añade un retraso de 1ns a las salidas 's' y 'cout' del módulo 'fa'.
      Compara las salidas de adder8_s o adder8_g con adder8 en este caso e interpreta
      los resultados. ¿Qué descripción es más realista, adder8_s o adder8?
*/
