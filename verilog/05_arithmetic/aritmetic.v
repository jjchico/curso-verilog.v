// Diseño:      aritmeticos
// Archivo:     arithmetic.v
// Descripción: Ejemplos de circuitos aritméticos en Verilog
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       10/12/2009 (versión inicial)

/*
   Unidad 5: Circuitos aritméticos

   En esta unidad veremos tres ejemplos de diseño de circuitos aritméticos:

   Lección 5.1 (adders.v): varias descripciones de sumadores a diversos
   niveles y comparación de sus resultados de simulación.

   Lección 5.2 (addsub.v): dos versiones alternativas de un sumador/restador
   en complemento a 2.

   Lección 5.3 (alu.v): diseño de una unidad lógica y aritmética capaz de
   realizar varias operaciones simples.

   Además, en esta unidad se introducen algunos elementos adicionales de
   Verilog como los operadores aritméticos y la directiva 'generate' y se
   practica la realización de descripciones estructurales y bancos de pruebas.
*/

module arithmetic();

    initial
        $display("Circuitos aritméticos.");

endmodule
