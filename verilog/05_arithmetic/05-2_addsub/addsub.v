// Diseño:      addsub
// Archivo:     addsub.v
// Descripción: Sumadores-restadores en complemento a 2
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       27/05/2010 (versión inicial)

/*
   Lección 5.2. Sumador-restador

   En esta lección haremos dos descripciones alternativas de un sumador/restador
   en complemento a 2 empleando operadores aritméticos, de forma parecida a como
   se hizo para el sumador en la lección anterior.

   En esta lección se introduce un concepto muy importante: las señales con
   signo. También se vuelve a ver la construcción 'parameter' para producir 
   un módulo que emplea datos de anchura arbitraria y se introduce la
   definición de variables locales en bloque de código con nombre.
*/

//////////////////////////////////////////////////////////////////////////
// Sumador-restador en complemento a dos versión 1                      //
//////////////////////////////////////////////////////////////////////////

/* La descripción del módulo sumador/restador es muy similar a la del sumador
 * visto en la lección anterior. El módulo incluye una entrada adicional 'op'
 * que determina el tipo de operación: 0-suma, 1-resta; y una salida de
 * desbordamiento ('ov').
 *
 * La anchura de los datos no está prefijada, sino que depende del parámetro
 * WIDTH. Este parámetro tiene un valor predeterminado de 8 indicado por la
 * directiva 'parameter', pero puede cambiarse a la hora de instanciar el
 * módulo, por lo que la misma descripción sirve para producir
 * sumadores/restadores de cualquier anchura de datos. En general, cuantas más
 * características de un módulo sean parametrizadas, mayor flexibilidad y
 * posibilidades de reutilización tendrá el módulo, por lo que el diseñador
 * debe prestar atención y usar parámetros siempre que sea posible.
 *
 * Las entradas 'a' y 'b', y la salida 'f' se declaran de tipo 'signed' por lo
 * que el simulador y el sintetizador asumen que estas señales contienen número
 * con signo en representación 'complemento a dos'. La declaración 'signed'
 * afecta a la interpretación del diseño de varias formas como, por  ejemplo, ls
 * visualización de datos con '$display', las operaciones de desplazamiento o la
 * extensión del número de bits en las asignaciones. */

module addsub1 #(
    parameter WIDTH = 8
    )(
    input wire signed [WIDTH-1:0] a,  // primer operando
    input wire signed [WIDTH-1:0] b,  // segundo operando
    input wire op,                    // operación (0-suma, 1-resta)
    output reg signed [WIDTH-1:0] f,  // salida
    output reg ov                     // desbordamiento
    );

    /* Recuerda: f y ov se declaran como tipo 'reg' (variables) porque
     * van a asignarse en un procedimiento 'always'. */

    always @* begin :sub
        /* Definimos una variable local al bloque para realizar la
         * suma con un bit adicional. La definición de varialbes
         * locales es posible sólo si se nombra el bloque ('sub' en
         * este caso). */
        reg signed [WIDTH:0] s;

        /* Aquí, la construcción 'if' hubiera sido igual de efectiva
         * que el 'case' pero, en general, cuando la decisión depende
         * de una sola variable (en este caso 'op') 'case' resulta más
         * claro, especialmente cuando el número de posibles valores
         * de la variable es elevado */
        case (op)
        0:
            s = a + b;
        default:
            s = a - b;
        endcase

        // Salida de desbordamiento
        /* 's' contiene el valor correcto de la operación. La extensión del
         * signo se realiza automáticamente ya que los tipos son 'signed',
         * esto es, si 'a+b' es positivo, el bit adicional de 's' se asignará
         * a '0', mientras que si es negativo se asignará a '1'. El
         * desbordamiento puede detectarse si comparamos el bit de signo del
         * resultado correcto 's[WIDTH]' con el bit de signo del resultado
         * sin extensión 's[WIDTH-1]' */
        if (s[WIDTH] != s[WIDTH-1])
            ov = 1;
        else
            ov = 0;

        // Salida
        f = s[WIDTH-1:0];
    end

endmodule // addsub1

//////////////////////////////////////////////////////////////////////////
// Sumador-restador en complemento a dos versión 2                      //
//////////////////////////////////////////////////////////////////////////

/* En la descripción de addsub1 se emplean variables con signo y se realizan
 * las operaciones 'a + b' o 'a - b' según corresponda. Según la inteligencia
 * de la herramienta de síntesis utilizada, esto puede producir un diseño
 * con un único bloque sumador/restador o un diseño con un sumador y un
 * restador independientes, que tiene mayor coste. Esta descripción alternativa
 * emplea tipos sin signo, por lo que es el diseñador directamente el que tiene
 * que gestionar la representación en complemento a dos. Esto significa que
 * para restar, se suma el operando 'b' complementado a uno más uno. Esta 
 * descripción (de más bajo nivel) requiere más esfuerzo por parte del 
 * diseñador y es más difícil de comprender y depurar, pero puede producir 
 * mejores resultados de síntesis. En general, el diseñador puede consultar la 
 * documentación de la herramienta de síntesis a utilizar en caso de que quiera
 * hacer una descripción óptima para la misma. */

module addsub2 #(
    parameter WIDTH = 8
    )(
    input wire [WIDTH-1:0] a,  // primer operando
    input wire [WIDTH-1:0] b,  // segundo operando
    input wire op,             // operación (0-suma, 1-resta)
    output reg [WIDTH-1:0] f,  // salida
    output reg ov              // desbordamiento
    );

    always @* begin :sub
        /* Variable temporal para calcular el segundo operando del sumador. */
        reg [WIDTH-1:0] c;

        /* Empleamos el operador condicional para calcular 'c', que
         * es un sustituto muy compacto de 'if'. En caso de resta 'c'
         * es el complemento a 1 (Ca1) de 'b' */
        c = op == 0 ? b : ~b;

        /* Calculamos el resultado. Añadimos 'op' para obtener el complemento
         * a dos de 'b' en caso de resta. */
        f = a + c + op;

        // Salida de desbordamiento
        /* Sólo se produce desbordamiento cuando se suman números del
         * mismo signo, y sólo si el signo aparente del resultado
         * difiere del de los operandos. */
        ov = ~a[WIDTH-1] & ~c[WIDTH-1] & f[WIDTH-1] |
             a[WIDTH-1] & c[WIDTH-1] & ~f[WIDTH-1];
    end

endmodule // addsub2

/*
   EJERCICIOS

   1. Compila la lección con:

        $ iverilog addsub.v

      Comprueba que no hay errores de sintaxis

   (continúa en 'addsub_tb.v')
*/
