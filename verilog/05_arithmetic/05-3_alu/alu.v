// Diseño:      alu
// Archivo:     alu.v
// Descripción: Unidad Aritmético-Lógica (ALU) sencilla
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       27/05/2010 (versión inicial)

/*
   Lección 5.3. Unidad aritmético-lógica

   En esta lección ampliaremos el sumador-restador diseñado en la Lección 5.2
   para consguir una Unidadr Aritmetico-Lógica (ALU) sencilla, a la vez que se
   introducen o se repasan diversos operadores lógicos y aritméticos. Las
   ALU están en el corazón de cualquier computador o procesador. Es la
   calculadora del procesador donde tienen lugar todos los cálculos
   necesarios para la ejecución de las instrucciones del procesador.
*/

//////////////////////////////////////////////////////////////////////////
// Unidad aritmético-logica (ALU)                                       //
//////////////////////////////////////////////////////////////////////////

/* La descripción de la ALU es similar a la del sumador/restador visto en la
 * lección anterior. La entrada de selección de operación se extiende para
 * indicar nuevas operaciones según la siguiente tabla:

 *   op[2:0]   Operation    z
 *   ----------------------------
 *       000    Suma         a + b
 *       001    Resta        a - b
 *       010    Incremento   a + 1
 *       011    Decremento   a - 1
 *       100    AND          a & b
 *       101    OR           a | b
 *       110    XOR          a ^ b
 *       111    NOT          ~a
 *
 * La anchura de los datos no está prefijada, sino que depende del parámetro
 * WIDTH. Este parámetro tiene un valor predeterminado de 8 indicado por
 * la directiva 'parameter', pero puede cambiarse a la hora de instanciar el
 * módulo, por lo que la misma descripción sirve para producir ALUs de
 * cualquier anchura de datos.
 *
 * La ALU opera con datos en complementdo a dos, por lo que se usan variables
 * de tipo 'signed'. La ALU no dispone de acarreo de entrada (cin), pero sí
 * dispone de salida de desbordamiento (ov). La salida 'ov' valdrá 1 cuando el
 * resultado en 'f' no sea correcto debido a desboradamiento (el resultado
 * ocupa más bits que la anchura de dato de la ALU).*/

module alu #(
    parameter WIDTH = 8
    )(
    input wire signed [WIDTH-1:0] a,    // primer operando
    input wire signed [WIDTH-1:0] b,    // segundo operando
    input wire [2:0] op,                // operación (0-suma, 1-resta)
    output reg signed [WIDTH-1:0] f,    // salida
    output reg ov                       // salida de desbordamiento (overflow)
    );

    always @* begin
        if (op[2] == 0) begin :arithm   // Operaciones aritméticas
            reg signed [WIDTH:0] s;
            /* El resto de los bits en 'op' seleccionan la operación
             * aritmética a realizar. El resultado es almacenado en 's' con
             * el posible bit adicional. */
            case (op[1:0])
            2'b00:
                s = a + b;  // suma
            2'b01:
                s = a - b;  // resta
            2'b10:
                s = a + 1;  // incremento
            2'b11:
                s = a - 1;  // decremento
            endcase

            // Cálculo del desbordamiento
            ov = (s[WIDTH] == s[WIDTH-1])? 0: 1;

            // Salida
            f = s[WIDTH-1:0];
        end else begin      // Operaciones lógicas
            case (op[1:0])
            2'b00:
                f = a & b;  // AND
            2'b01:
                f = a | b;  // OR
            2'b10:
                f = a ^ b;  // XOR
            2'b11:
                /* '~' es el operador 'complemento' que invierte
                 * todos los bits del operando. */
                f = ~a;     // NOT
            endcase
            // No hay overflow en las operaciones lógicas
            ov = 0;
        end // if
    end // always

endmodule // alu

/*
   EJERCICIOS

   1. Compila la lección con:

        $ iverilog alu.v

      Comprueba que no hay errores de sintaxis.

   (continúa en alu_tb.v)
*/
